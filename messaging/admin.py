# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Message
# Register your models here.


class MessageAdmin(admin.ModelAdmin):
    model = Message
    date_hierarchy = 'created_on'
    list_display = ('author', 'recipient', 'is_read', 'created_on')
    list_display_links = ('author', 'recipient', 'created_on')
    list_editable = ('is_read',)
    list_filter = ('is_read', 'author__user__username', 'content')
    ordering = ('created_on',)
    fieldsets = (
        (None, {
            'fields': ('author', 'recipient', 'content', 'picture')
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('is_read', 'duration'),
            'description': 'Fill the advanced options'
        }),
    )

admin.site.register(Message, MessageAdmin)
