import os

from model.model import User, Model, Software, Base, engine

from classes.controller.userController     import UserController     as UsrCtrl
from classes.controller.modelController    import ModelController    as MdlCtrl
from classes.controller.softwareController import SoftwareController as SftCtrl

class Menu:

    def __init__(self):
        self.do = True

        while self.do:
            self.mainMenu()

    def clear(self):
        # Ne marche pas si le script est lancé avec le shell python
        # os.system('cls' if os.name == 'nt' else 'clear')
        pass

    def pause(self):
        input("Appuyez sur une touche pour continuer")

    def inputNumber(self, text):
        while 1 == 1:
            try:
                choice = int(input(text))
                return choice

            except ValueError:
                print("Ce n'est pas un nombre")

    def inputString(self, text):
        while 1 == 1:
            string = input(text)
            if len(string) > 0:
                return string

    def mainMenu(self):
        self.clear()

        print()
        print("###########################")
        print("# 3D Swap -- Mode console #")
        print("###########################")
        print("")
        print("1. Utilisateurs")
        print("2. Modèles")
        print("3. Logiciels")
        print("")
        print("4. Quitter")
        print("")

        choice = self.inputNumber("Choix : ")
 
        if choice == 1:
            self.usersMenu()
        elif choice == 2:
            self.modelsMenu()
        elif choice == 3:
            self.softwaresMenu()
        else:
            self.do = False

    def usersMenu(self):
        do = True

        while do:
            self.clear()

            print()
            print("################")
            print("# Utilisateurs #")
            print("################")
            print("")
            print("1. Liste")
            print("2. Ajouter")
            print("3. Supprimer")
            print("")
            print("4. Retour")
            print("")

            choice = self.inputNumber("Choix : ")

            if choice == 1:
                self.listUsers()
            elif choice == 2:
                self.addUser()
            elif choice == 3:
                self.deleteUser()
            else:
                do = False

    def modelsMenu(self):
        self.clear()

        do = True

        while do:
            self.clear()

            print()
            print("###########")
            print("# Modèles #")
            print("###########")
            print("")
            print("1. Liste")
            print("2. Supprimer")
            print("")
            print("4. Retour")
            print("")

            choice = self.inputNumber("Choix : ")

            if choice == 1:
                self.listModels()
            elif choice == 2:
                self.deleteModel()
            elif choice == 3:
                pass
            else:
                do = False

    def softwaresMenu(self):
        self.clear()

        do = True

        while do:
            self.clear()

            print()
            print("#############")
            print("# Logiciels #")
            print("#############")
            print("")
            print("1. Liste")
            print("2. Ajouter")
            print("3. Supprimer")
            print("")
            print("4. Retour")
            print("")

            choice = self.inputNumber("Choix : ")

            if choice == 1:
                self.listSoftwares()
            elif choice == 2:
                self.addSoftware()
            elif choice == 3:
                self.deleteSoftware()
            else:
                do = False
            
    def listUsers(self):
        users = UsrCtrl.getAll()
        print("")
        print("Il y a {} utilisateur(s) :".format(len(users)))
              
        for user in users:
            print("\tid:{}\tpseudo:{}\tpassword:{}".format(user.id, user.pseudonym, user.password))
        
        self.pause()

    def addUser(self):
        print("")
        surname = self.inputString("Nom    : ")
        name    = self.inputString("Prénom : ")

        valid = False
        while not valid:
            pseudo   = self.inputString("Pseudo : ")
            valid = not UsrCtrl.pseudoExists(pseudo)

        valid = False
        while not valid:
            mail  = self.inputString("Mail   : ")
            valid = not UsrCtrl.mailExists(mail)

        password = self.inputString("Mdp    : ")

        user = User(surname, name, pseudo, mail, password)

        UsrCtrl.add(user)

        print("Utilisateur ajouté")
        self.pause()

    def deleteUser(self):
        print("")

        id = self.inputNumber("Id :")

        user = UsrCtrl.getOne(id)
        if (user == None):
            print("L'utilisateur n'existe pas")
        else:
            print("Utilisateur sélectionné :")
            print("id:{}\tpseudo:{}\tpassword:{}".format(user.id, user.pseudonym, user.password))
            
            choice = self.inputString("Valider ? (y/n) :")

            if choice == "y":
                UsrCtrl.delete(user)
                print("Utilisateur supprimé")


        self.pause()

    def listModels(self):
        models = MdlCtrl.getAll()
        print("")
        print("Il y a {} model(s) :".format(len(models)))
              
        for model in models:
            print("\tid:{}\tlabel:{}\tuser:{}".format(model.id, model.label, model.getUser().pseudonym))
        
        self.pause()

    def addModel(self):
        self.pause()

    def deleteModel(self):
        print("")

        id = self.inputNumber("Id :")

        model = MdlCtrl.getOne(id)
        if (model == None):
            print("Le modèle n'existe pas")
        else:
            print("Modèle sélectionné :")
            print("\tid:{}\tlabel:{}\tuser:{}".format(model.id, model.label, model.getUser().pseudonym))
            
            choice = self.inputString("Valider ? (y/n) :")

            if choice == "y":
                MdlCtrl.delete(model)
                print("Modèle supprimé")

        self.pause()

    def listSoftwares(self):
        softwares = SftCtrl.getAll()
        print("")
        print("Il y a {} logiciel(s) :".format(len(softwares)))
              
        for software in softwares:
            print("\tid:{}\tname:{}".format(software.id, software.name))
        
        self.pause()
        

    def addSoftware(self):
        print("")

        valid = False
        while not valid:
            name = self.inputString("Nom : ")
            valid = not SftCtrl.nameExists(name)

        software = Software(name)

        SftCtrl.add(software)

        print("Logiciel ajouté")
        self.pause()

    def deleteSoftware(self):
        print("")

        id = self.inputNumber("Id :")

        software = SftCtrl.getOne(id)
        if (software == None):
            print("Le logiciel n'existe pas")
        else:
            print("Logiciel sélectionné :")
            print("id:{}\tname:{}".format(software.id, software.name))
            
            choice = self.inputString("Valider ? (y/n) :")

            if choice == "y":
                SftCtrl.delete(software)
                print("Logiciel supprimé")

        self.pause()

Menu()
