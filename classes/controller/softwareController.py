from sqlalchemy.orm import sessionmaker

from model.model import Software, Base, engine, session

Base.metadata.bind = engine

class SoftwareController:

    @staticmethod   
    def add(software):
        session.add(software)
        session.commit()

    @staticmethod
    def addAll(softwares):
        session.add_all(softwares)
        session.commit()

    @staticmethod
    def getOne(id):
        return session.query(Software).filter_by(id = id).first()

    @staticmethod
    def getAll():
        return session.query(Software).all()

    @staticmethod
    def delete(software):
        session.delete(software)
        session.commit()

    @staticmethod
    def nameExists(name):
        return session.query(Software).filter_by(name = name).count()