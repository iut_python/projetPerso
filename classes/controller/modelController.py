from sqlalchemy.orm import sessionmaker

from model.model import Model, Base, engine, session

Base.metadata.bind = engine

class ModelController:

    @staticmethod   
    def add(model):
        session.add(model)
        session.commit()

    @staticmethod
    def addAll(users):
        session.add_all(users)
        session.commit()

    @staticmethod
    def getOne(id):
        return session.query(Model).filter_by(id = id).first()

    @staticmethod
    def getAll():
        return session.query(Model).all()

    @staticmethod
    def delete(model):
        session.delete(model)
        session.commit()