from sqlalchemy.orm import sessionmaker

from model.model import User, Base, engine, session

Base.metadata.bind = engine

class UserController:

    @staticmethod   
    def add(user):
        session.add(user)
        session.commit()

    @staticmethod
    def addAll(users):
        session.add_all(users)
        session.commit()

    @staticmethod
    def getOne(id):
        return session.query(User).filter_by(id = id).first()

    @staticmethod
    def getAll():
        return session.query(User).all()

    @staticmethod
    def delete(user):
        session.delete(user)
        session.commit()

    @staticmethod
    def pseudoExists(pseudo):
        return session.query(User).filter_by(pseudonym = pseudo).count()

    @staticmethod
    def mailExists(mail):
        return session.query(User).filter_by(mail = mail).count()