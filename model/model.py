from sqlalchemy import Table, Column, String, Integer, Boolean, ForeignKey, Date, create_engine

from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from datetime import datetime
import hashlib

Base   = declarative_base()
engine = create_engine('sqlite:///bd.sql3')

Session = sessionmaker(bind = engine)
session = Session()

model_software = Table("model_software", Base.metadata,
    Column("model", Integer, ForeignKey("model.id")),
    Column("software", Integer, ForeignKey("software.id"))
)

class User(Base):
    __tablename__ = "user"

    id        = Column(Integer, primary_key = True)
    surname   = Column(String)
    name      = Column(String)
    pseudonym = Column(String, nullable = False)
    mail      = Column(String)
    password  = Column(String, nullable = False)

    models    = relationship("Model")

    def __init__(self, surname, name, pseudonym, mail, password):
        self.surname   = surname
        self.name      = name
        self.pseudonym = pseudonym
        self.mail      = mail
        self.password  = hashlib.sha512(password.encode('utf-8')).hexdigest()

    def getModels(self):
        return session.query(Model).filter_by(user = self.id).all()

class Model(Base):
    __tablename__ = "model"
    
    id          = Column(Integer, primary_key = True)
    label       = Column(String, nullable = False)
    description = Column(String)
    submission  = Column(Date, default = datetime.now().date())
    tags        = Column(String, nullable = False)
    size        = Column(Integer, nullable = False)
    vertices    = Column(Integer, nullable = False)
    faces       = Column(Integer, nullable = False)
    textured    = Column(Boolean, nullable = False)
    filePath    = Column(String, nullable = False)
    fileType    = Column(String, nullable = False)

    user        = Column(Integer, ForeignKey("user.id"))
    softwares    = relationship(
        "Software",
        secondary = model_software,
        back_populates = "models")

    def __init__(self, label, description, tags, size, vertices, faces, textured, user):
        self.label       = label
        self.description = description
        self.tags        = tags
        self.size        = size
        self.vertices    = vertices
        self.faces       = faces
        self.textured    = textured
        self.filePath    = filePath
        self.fileType    = fileType
        self.user        = user

    def getUser(self):
        return session.query(User).filter_by(id = self.user).first()

    def getSoftware(self):
        return session.query(Software).filter_by(id = self.software).first()

class Software(Base):
    __tablename__ = "software"

    id     = Column(Integer, primary_key = True)
    name   = Column(String, nullable = False)

    models = relationship(
        "Model",
        secondary = model_software,
        back_populates = "softwares")

    def __init__(self, name):
        self.name = name

    def getModels(self):
        return session.query(Model).filter_by(software = self.id).all()

Base.metadata.create_all(engine)
