<%inherit file="/layouts/base.mako"/>
<div class="content row">

	<div class="col-sm-10 col-md-10 col-centered">

		<div class="row">
			<article class="col-md-7 introduction">
				<h1>3D Swap</h1>
				<hr/>
				<p>
					<i>3D Swap</i> is a community of passionate 3D artists who share their work under creative commons licenses.
				</p>
			</article>

			<div class="col-md-1">
			</div>

			<div class="col-md-4">
				<div class="row">
					%if user == None:
						<article class="col-md-12">
							<h2>Account</h2>
							<hr/>
							<a href="/user/login">Sign in</a> or <a href="/user/signup">create an account</a>
						</article>
					%else:
						<article class="col-md-12">
							<h2>Your models</h2>
							<hr/>
							%if len(user.models) > 0:
								<ul class="model-list">
									%for model in user.models:
										<li><a href="/model/see/${model.id}">${model.label}</a><span class="count"> (${model.hits} downloads)</span> <a href="/model/edit?id=${model.id}">edit</a></li>
									%endfor
								</ul>
							%else:
								<p>You dont have any model yet, <a href="/model/add">post one</a>!</p>
							%endif

						</article>
					%endif;
					<article class="col-md-12">
						<h2>Tags</h2>
						<hr/>
						%for tag in tags:
							<a href="/model/search?s=${tag.tag}">#${tag.tag}</a><sub>(${tag.count})</sub>
						%endfor
					</article>
				</div>
			</div>
		</div>
	</div>
</div>
