<%inherit file="/layouts/base.mako"/>

<div class="content row">
	<div class="form-1 col-centered">
		<h1 class="center">Login</h1>
		<hr/>
		<form action="login" method="post">
			<div class="form-main-message"></div>
			<div class="main-form">
				<div class="login-group">
					<div class="form-group">
						<label for="pseudonym" class="sr-only">Username</label>
						%if pseudonym is UNDEFINED:
							<input type="text" class="form-control" name="pseudonym"placeholder="pseudonym"/>
						%else:
							<input type="text" class="form-control" name="pseudonym" value="${pseudonym}" placeholder="pseudonym"/>
						%endif
					</div>
					<div class="form-group">
						<label for="password" class="sr-only">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="password">
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			<p class="center">or <a href="/user/signup">create new account</a></p>
		</form>
		% if not messages is UNDEFINED and len(messages) > 0:
			<div class="error">
				<ul>
					%for message in messages:
						<li>${message}</li>
					%endfor
				</ul>
			</div>
		%endif
	</div>
</div>
