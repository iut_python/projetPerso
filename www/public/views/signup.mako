<%inherit file="/layouts/base.mako"/>


<div class="content row">
	<div class="form-1 col-centered">
		<h1 class="center">Sign up</h1>
		<hr/>
		<form action="signup" method="post">
			<div class="form-main-message"></div>
			<div class="main-form">
				<div class="login-group">
					<div class="form-group">
						<input type="text" class="form-control" name="name" value="${name}" placeholder="name"/>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="surname" value="${surname}" placeholder="surname"/>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="pseudonym" value="${pseudonym}" placeholder="* pseudonym"/>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="mail" value="${mail}" placeholder="* e-mail"/>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="password" value="" placeholder="* password"/>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" name="passwordConf" value="" placeholder="* password confirmation"/>
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			<p class="center">or <a href="/user/login">login here</a></p>
		</form>
		% if not messages is UNDEFINED and len(messages) > 0:
			<div class="error">
				<ul>
					%for message in messages:
						<li>${message}</li>
					%endfor
				</ul>
			</div>
		%endif
	</div>
</div>
