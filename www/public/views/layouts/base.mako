<html lang="en">
	<head>
		<meta charset="utf-8"/>
		<title>3D Swap - ${title}</title>
		<link rel="stylesheet" type="text/css" href="/lib/normalize-4.1.1.css"/>
		<link rel="stylesheet" type="text/css" href="/lib/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>
		<link rel="stylesheet" type="text/css" href="/lib/font-awesome-4.6.2/css/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="/lib/bootstrap-multiselect-0.9.13/dist/css/bootstrap-multiselect.css"/>
		<link rel="stylesheet" type="text/css" href="/lib/select2-4.0.2/dist/css/select2.min.css"/>
		<link rel="stylesheet" type="text/css" href="/css/main.css"/>
		<link rel="stylesheet" type="text/css" href="/css/nav.css"/>
		<link rel="stylesheet" type="text/css" href="/css/form.css"/>
		<link rel="stylesheet" type="text/css" href="/css/bootstrap-overwrite.css"/>

		<script src="/lib/jquery-1.12.3.min.js"></script>
		<script src="/lib/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
		<script src="/lib/bootstrap-multiselect-0.9.13/dist/js/bootstrap-multiselect.js"></script>
		<script src="/lib/select2-4.0.2/dist/js/select2.min.js"></script>
	</head>

	<body>
		<div class="page">
			<div class="filter"></div>
			<div class="parallax"></div>
			<nav id="menu">
				<ul>
					<li class="left"><a href="/"><i class="fa fa-home" aria-hidden="true"></i></a></li>
					<li class="left"><a href="/model/"><i class="fa fa-cubes"></i> Models</a></li>

					<li class="right">
						%if user == None:
							<a href="/user/login"><i class="fa fa-user"></i> Login</a>
							<ul>
								<li><a href="/user/signup">Sign up</a></li>
							</ul>
						%else:
							<a href="/user/self"><i class="fa fa-user"></i> ${user.pseudonym}</a>
							<ul>
								<li><a href="/model/add">Add a model</a></li>
								<li><a href="/user/logout">Logout</a></li>
							</ul>
						%endif
					</li>
					<li class="right search">
						<div class="searchBar">
							<form action="/model/search" method="GET">
								<input type="text" name="s" value="${searched}" placeholder="Search" />
							</form>
						</div>
					</li>
				</ul>

			</nav>
			${next.body()}
		</div>
		<footer>
			<i class="fa fa-copyright" aria-hidden="true"></i> 3D Swap - 2k16
		</footer>
	</body>
</html>
<script type="text/javascript">
	backgrounds = ${backgrounds};
	img = new Image();

	img.onload = function() {
		$(".parallax").css("background-image","url("+this.src+")");
		console.log(this.src);
	}

	img.src = '/img/default_backgrounds/'+backgrounds[getRandomInt(0, backgrounds.length)];

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	function parallax(){
		var pageHeight = $(document).height();
		var scrolled   = $(window).scrollTop();
		var value      = scrolled * 0.05;

		$('.parallax').css('background-position', '0 -'+value+'px');
	}

	$(window).scroll(function(e){
		parallax();
	});

</script>
