<%! from includes.size import size, alternative %>
<%inherit file="/layouts/base.mako"/>
<div class="content row">

	<div class="col-sm-10 col-centered">
		<div class="row">

			<div class="col-sm-12 col-md-6">
				<article>
					<h1>${model.label}</h1>
					<hr/>
					<figure>
						<img src="${model.images}" width="100%" />
					</figure>
					<hr/>
					<span class="tags">
						<p>
							Tags :&nbsp;
							%for tag in model.tags:
								<a href="/model/search/${tag.tag}">#${tag.tag}</a><span class="count"><sub>(${tag.count})</sub></span>
							%endfor
						</p>
					</span>
				</article>
			</div>

			<div class="col-sm-12 col-md-6">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<article>
							<h2>Details</h2>
							<hr  />
							<p>
								<table class="details">
									<tr>
										<td><span class="title">Made with</span></td>
										<td><span class="title">&nbsp;&nbsp;:&nbsp;&nbsp;</span></td>
										<td>
											%for software in model.softwares:
												<a href="/model/search/${software.base}">${software.name}</a>&nbsp;
											%endfor
										</td>
									</tr>
									<tr>
										<td><span class="title">Vertices</span></td>
										<td><span class="title">&nbsp;&nbsp;:&nbsp;&nbsp;</span></td>
										<td>${model.vertices}</td>
									</tr>
									<tr>
										<td><span class="title">Faces</span></td>
										<td><span class="title">&nbsp;&nbsp;:&nbsp;&nbsp;</span></td>
										<td>${model.faces}</td>
									</tr>
									<tr>
										<td><span class="title">Textured</span></td>
										<td><span class="title">&nbsp;&nbsp;:&nbsp;&nbsp;</span></td>
										<td>
											%if model.textured:
												Yes
											%else:
												No
											%endif
										</td>
									</tr>
								</table>
							</p>
						</article>
					</div>

					<div class="col-sm-12 col-md-12">
						<article>
							<h2>Files</h2>
							<hr/>
							<div class="row">
								%for file in model.files:
									<div class="col-sm-12 col-md-6">
										<a href="/model/download/${file.id}" target="_blank" class="download-button">
											<span class="software">
												${file.getFileType().label}
											</span>

											<span>
												${size(file.size, system = alternative)}<br/>
												${file.hits} downloads
											</span>
										</a>
									</div>
								%endfor
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
