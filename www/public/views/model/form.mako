<%inherit file="/layouts/base.mako" />

<div class="content row">
	<div class="col-sm-10 col-md-10 col-centered">
		<div class="row">

			<form method="post" action="${action}" enctype="multipart/form-data">
				<input type="hidden" name="files" value="[]" />
				<input type="hidden" name="submitted" value="1"/>

				<div class="col-sm-12 col-md-12">
					<article class="row">

						<div class="col-md-7">
							<div class="form-add">
								<h2>New model</h2>
								<hr/>

								<div class="form-group">
									<label for="label">Label *</label>
									<input type="text" class="form-control" name="label" value="${label}" />
								</div>

								<div class="form-group">
									<label for="description">Description *</label>
									<textarea class="form-control" name="description">${description}</textarea>

								</div>

								<div class="form-group">
									<label for="tags">Tags *</label>
									<input type="text" class="form-control" name="tags" placeholder="tag1, tag_2, ..." value="${tags}" />
								</div>
							</div>

							<div class="form-add">
								<h2>Details</h2>
								<hr/>

								<div class="col-sm-12 col-md-6">
									<div class="form-group">
										<label for="vertices">Vertices</label>
										<input type="text" class="form-control" name="vertices" value="${vertices}" />
									</div>
								</div>

								<div class="col-sm-12 col-md-6">
									<div class="form-group">
										<label for="vertices">Faces</label>
										<input type="text" class="form-control" name="faces" value="${faces}" />
									</div>
								</div>

								<div class="form-group login-group-checkbox">
									<input id="textured" name="textured" type="checkbox">
									<label for="textured">has textures</label>
								</div>

								<div class="form-group">
									<label for="softwares">Made with</label>
									<select id="softwares" name="softwares" class="form-control" multiple="multiple">
										%for software in allSoftwares:
											%if str(software.id) in softwares and len(softwares):
												<option value="${software.id}" selected="true">${software.name}</option>
											%else:
												<option value="${software.id}">${software.name}</option>
											%endif
										%endfor
									</select>
								</div>
							</div>

							<div class="form-add">
								<h2>File types</h2>
								<hr/>
								<label for="fileTypes">Available file types (needs to update to apply file change)</label>
								<select id="fileTypes" name="fileTypes" class="form-control" multiple="multiple">
									%for fileType in allFileTypes:
										%if str(fileType.id) in fileTypes and len(fileTypes):
											<option value="${fileType.id}" selected="true">${fileType.label}</option>
										%else:
											<option value="${fileType.id}">${fileType.label}</option>
										%endif
									%endfor
								</select>

								<table>
									%for file in files:
										<tr>
											<td>${file.getFileType().label}</td>
											<td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
											<td>${file.path}</td>
											<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
											<td><input type="file" name="file['${file.getFileType().id}']" /></td>
										</tr>
									%endfor
								</table>
							</div>

						</div>

						<div class="col-sm-12 col-md-5">

							<div class="col-md-12">
								<div class="form-add">
									<h2>Image</h2>
									<hr/>

									<div class="form-group">
										<figure>
												<img src="${finalImage}" />
										</figure>
										<hr/>
										<input type="file" name="images" />
									</div>
								</div>
							</div>

							<div class="col-md-12"></div>


						</div>

						<div class="col-sm-12 col-md-7">
							%if id == 0:
								<input type="submit" class="button-add" value="Add" />
							%else:
								<input type="submit" class="button-add" value="Update" />
							%endif

							% if not messages is UNDEFINED and len(messages) > 0:
								<div class="error">
									<ul>
										%for message in messages:
											<li>${message}</li>
										%endfor
									</ul>
								</div>
							%endif
						</div>
						<div class="col-sm-12 col-md-5">
							%if not id == 0:
								<a class="button-delete" href="/model/delete/${id}">Delete this model</a>
							%endif
						</div>

					</article>
				</div>

			</form>
			${elements}
		</div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#softwares').select2();
        $('#fileTypes').select2();
    });
</script>
