<%inherit file="/layouts/base.mako"/>
<div class="content row">
	<article class="col-sm-10 col-md-10 col-centered">
		<h1 class="center">${contentTitle}</h1>
		<hr/>
		<div class="row">
			%for model in models:
				<div class="col-sm-12 col-md-6">
					<div class="model row">
						<figure>
							<img height="200px" src="${model.images}"/>
						</figure>
						<aside>
							<h2>
								${model.label}
								<span>made by&nbsp;<span class="author">${model.getUser().pseudonym}</span></span>
							</h2>
							<hr/>
							<p>
								${model.description}
							</p>
							<a href="/model/see/${model.id}" class="button"><i class="fa fa-angle-double-right" aria-hidden="true"></i> See</a>
						</aside>
					</div>
				</div>
			%endfor
		</div>
	</article>
</div>
