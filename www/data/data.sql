
-- Softwares
INSERT INTO software (id, name, base) VALUES (1, 'Blender 2.60', 'blender');
INSERT INTO software (id, name, base) VALUES (2, 'Blender 2.61', 'blender');
INSERT INTO software (id, name, base) VALUES (3, 'Blender 2.62', 'blender');
INSERT INTO software (id, name, base) VALUES (4, 'Blender 2.63', 'blender');
INSERT INTO software (id, name, base) VALUES (5, 'Blender 2.64', 'blender');
INSERT INTO software (id, name, base) VALUES (6, 'Blender 2.65', 'blender');
INSERT INTO software (id, name, base) VALUES (7, 'Blender 2.66', 'blender');
INSERT INTO software (id, name, base) VALUES (8, 'Blender 2.67', 'blender');
INSERT INTO software (id, name, base) VALUES (9, 'Blender 2.68', 'blender');
INSERT INTO software (id, name, base) VALUES (10, 'Blender 2.69', 'blender');
INSERT INTO software (id, name, base) VALUES (11, 'Blender 2.70', 'blender');
INSERT INTO software (id, name, base) VALUES (12, 'Blender 2.71', 'blender');
INSERT INTO software (id, name, base) VALUES (13, 'Blender 2.72', 'blender');
INSERT INTO software (id, name, base) VALUES (14, 'Blender 2.73', 'blender');
INSERT INTO software (id, name, base) VALUES (15, 'Blender 2.74', 'blender');
INSERT INTO software (id, name, base) VALUES (16, 'Blender 2.75', 'blender');
INSERT INTO software (id, name, base) VALUES (17, 'Blender 2.76', 'blender');
INSERT INTO software (id, name, base) VALUES (18, 'Blender 2.77', 'blender');
INSERT INTO software (id, name, base) VALUES (19, 'Maya 2011', 'maya');
INSERT INTO software (id, name, base) VALUES (20, 'Maya 2012', 'maya');
INSERT INTO software (id, name, base) VALUES (21, 'Maya 2013', 'maya');
INSERT INTO software (id, name, base) VALUES (22, 'Maya 2014', 'maya');
INSERT INTO software (id, name, base) VALUES (23, 'Maya 2014 SP1', 'maya');
INSERT INTO software (id, name, base) VALUES (24, 'Maya 2015', 'maya');
INSERT INTO software (id, name, base) VALUES (25, 'Maya 2015 SP1', 'maya');
INSERT INTO software (id, name, base) VALUES (26, 'Maya 2015 SP2', 'maya');
INSERT INTO software (id, name, base) VALUES (27, 'ZBrush 3.5', 'zbrush');
INSERT INTO software (id, name, base) VALUES (28, 'ZBrush 4.0', 'zbrush');
INSERT INTO software (id, name, base) VALUES (29, 'ZBrush 4.1', 'zbrush');
INSERT INTO software (id, name, base) VALUES (30, 'ZBrush 4.2', 'zbrush');
INSERT INTO software (id, name, base) VALUES (31, 'ZBrush 4R7', 'zbrush');

-- Users
INSERT INTO user (id, surname, name, pseudonym, mail, password) VALUES (1, 'Bourgeat', 'Aleksandre', 'pony', 'pony@pony.com', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413');

-- Models
INSERT INTO model (id, label, description, vertices, faces, textured, images, hits, user) VALUES (1, 'Model 1', 'Model 1 description', 40, 10, 0, '/model/img/default.jpg', 0, 1);

-- Tags
INSERT INTO tag (id, tag, count) VALUES (1, 'robot', 1);
INSERT INTO tag (id, tag, count) VALUES (2, 'blender', 1);

-- File types
INSERT INTO file_type (id, label) VALUES (1, "collada<span>*.dae</span>");
INSERT INTO file_type (id, label) VALUES (2, "blender<span>*.blend</span>");
INSERT INTO file_type (id, label) VALUES (3, "wavefront<span>*.obj, *.mtl</span>");
INSERT INTO file_type (id, label) VALUES (4, "zbrush<span>*.zbr, *.ztl</span>");

-- Files
INSERT INTO file (id, fileType, path, model, size, hits) VALUES (1, 2, "/model/file/robot.blend", 1, 1084036, 0);
INSERT INTO file (id, fileType, path, model, size, hits) VALUES (2, 3, "/model/file/robot_obj.zip", 1, 573440, 0);

-- Model / Software relation
INSERT INTO model_software (model, software) VALUES (1, 18);

-- Model / Tag relation
INSERT INTO model_tag (model, tag) VALUES (1, 1);
INSERT INTO model_tag (model, tag) VALUES (1, 2);
