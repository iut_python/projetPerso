import os
import uuid

import cherrypy

from includes.model.model import User, Model, Software, Tag

from includes.classes.controller.userController     import UserController     as UsrCtrl
from includes.classes.controller.modelController    import ModelController    as MdlCtrl
from includes.classes.controller.softwareController import SoftwareController as SftCtrl
from includes.classes.controller.fileController     import FileController     as FileCtrl
from includes.classes.controller.tagController      import TagController      as TagCtrl

from includes.templates  import serve
from includes.decorators import *

from controllers.base import Base

config = os.path.join(os.path.dirname(__file__), 'config/server.conf')

class ModelController(Base):

    @cherrypy.expose
    def index(self):
        models = MdlCtrl.getAll()

        return super(ModelController, self).render(
            'model/list',
            title        = 'Models',
            contentTitle = 'All models',
            page         = 1,
            search       = '',
            models       = models)

    @cherrypy.expose
    def search(self, s):

        if len(s) > 0:
            tags   = s.split()
            models = MdlCtrl.getTagged(tags)

            return super(ModelController, self).render(
                'model/list',
                title        = 'Search results',
                contentTitle = '<span>Results for</span><br/>'+s,
                page         = 1,
                search       = s,
                models       = models,
                tags = tags)
        else:
            return self.index()

    @cherrypy.expose
    def see(self, id):
        model = MdlCtrl.getOne(id)

        return super(ModelController, self).render(
            'model/see',
            title        = model.label,
            contentTitle = '',
            model        = model)

    @cherrypy.expose
    def download(self, id):
        file = FileCtrl.getOne(id)
        file.hits = file.hits + 1


        model = MdlCtrl.getOne(file.model)
        model.hits = model.hits + 1

        FileCtrl.update(file)
        MdlCtrl.update(model)
        raise cherrypy.HTTPRedirect(file.path)

    @cherrypy.expose
    @requireAuthentication
    def add(self, **kwargs):

        messages = []

        allSoftwares = SftCtrl.getAll()
        allFileTypes = FileCtrl.getAllTypes()

        id = 0
        label       = kwargs['label']       if 'label'       in kwargs else ''
        description = kwargs['description'] if 'description' in kwargs else ''
        tags        = kwargs['tags']        if 'tags'        in kwargs else ''
        vertices    = kwargs['vertices']    if 'vertices'    in kwargs else 0
        faces       = kwargs['faces']       if 'faces'       in kwargs else 0
        textured    = kwargs['textured']    if 'textured'    in kwargs else 0
        images      = kwargs['images']      if 'images'      in kwargs else ''
        softwares   = kwargs['softwares']   if 'softwares'   in kwargs else []

        aFileTypes = []
        if 'fileTypes' in kwargs:
            fileTypes = kwargs['fileTypes']

            for fileType in fileTypes:
                aFileTypes.append(FileCtrl.getOneType(fileType))

        # Image shown when none is uploaded
        finalImage = '/model/img/default.jpg'

        fileTypes  = []

        files = []

        # If the form was submitted
        if 'submitted' in kwargs:

            # Check if required elements are filled
            if len(label) and len(description) and len(tags):

                aTags      = []
                aSoftwares = []

                if len(kwargs['images'].filename) > 0:
                    path = cherrypy.request.app.config['/']['tools.staticdir.root'] + "/public/assets/model/img/"
                    extension = os.path.splitext(images.filename)[1]
                    imageName = str(uuid.uuid4())+extension

                    # Write the file
                    with open(path+imageName, 'wb') as out:
                        out.write(images.file.read())

                    finalImage = '/model/img/' + imageName

                # Load tags
                for tag in tags.split(','):
                    if TagCtrl.tagExists(tag.strip()):
                        aTags.append(TagCtrl.getOneByTag(tag.strip()))
                    else:
                        # Create the tag if it does not exist
                        newTag = Tag(tag.strip(), 1)
                        TagCtrl.add(newTag)
                        aTags.append(newTag)

                # Load the softwares
                for software in softwares:
                    aSoftwares.append(SftCtrl.getOne(software))

                # Build the model object
                model = Model()
                model.label = label
                model.description = description
                model.tags = aTags
                model.vertices = vertices
                model.faces = faces
                model.textured = textured
                model.images = finalImage
                model.softwares = aSoftwares
                model.user = cherrypy.session['user'].id

                # Save the entity
                MdlCtrl.add(model)

                # Update the tag count & delete them if necessary
                TagCtrl.countTags()

                id = model.id
            else:
                messages.append('Required fields are not provided')


        if len(messages) == 0 and id > 0:
            raise cherrypy.HTTPRedirect('/model/edit?id=' + str(id))

        else:
            return super(ModelController, self).render(
                'model/form',
                title        = 'New model',
                id           = id,
                label        = label,
                description  = description,
                tags         = tags,
                vertices     = vertices,
                faces        = faces,
                textured     = textured,
                images       = images,
                finalImage   = finalImage,
                softwares    = softwares,
                allSoftwares = allSoftwares,
                allFileTypes = allFileTypes,
                aFileTypes   = aFileTypes,
                fileTypes    = fileTypes,
                files        = files,
                elements     = kwargs,
                messages     = messages,
                action       = '/model/add')

    @requireAuthentication
    @cherrypy.expose
    def edit(self, **kwargs):
        if 'id' in kwargs:
            messages = []

            model = MdlCtrl.getOne(kwargs['id'])

            allSoftwares = SftCtrl.getAll()
            allFileTypes = FileCtrl.getAllTypes()

            softwares = []
            if 'softwares' in kwargs:
                if isinstance(kwargs['softwares'], list):
                    for software in kwargs['softwares']:
                        softwares.append(software)
                elif len(kwargs['softwares']) > 0:
                    softwares.append(kwargs['softwares'])
            else:
                for software in model.softwares:
                    softwares.append(str(software.id))

            tags = ''
            for tag in model.tags:
                tags += tag.tag + ","
            tags = tags.strip(',')

            fileTypes  = []
            aFileTypes = []
            if 'fileTypes' in kwargs:
                fileTypes = kwargs['fileTypes']

                for fileType in fileTypes:
                    aFileTypes.append(FileCtrl.getOneType(fileType))
            else:
                for file in model.files:
                    fileTypes.append(str(file.fileType))
                    aFileTypes.append(FileCtrl.getOneType(file.fileType))

            files = model.files

            finalImage = model.images

            if 'submitted' in kwargs:

                tags = kwargs['tags']

                if len(kwargs['images'].filename) > 0:
                    static   = cherrypy.request.app.config['/']['tools.staticdir.root']
                    absolute = static + '/public/assets/model/img'

                    # Remove the current image
                    if not model.images == '/model/img/default.jpg' and os.path.isfile(static + '/public/assets' + model.images) :
                        os.remove(static + '/public/assets' + model.images)

                    extension = os.path.splitext(kwargs['images'].filename)[1]
                    imageName = str(uuid.uuid4()) + extension

                    # Write the file
                    with open(absolute + '/' + imageName, 'wb') as out:
                        out.write(kwargs['images'].file.read())

                    finalImage = '/model/img/' + imageName
                    model.images = finalImage

                aTags      = []
                aSoftwares = []
                textured   = 'tagExists' in kwargs

                # Load tags
                for tag in tags.split(','):
                    if TagCtrl.tagExists(tag.strip()):
                        aTags.append(TagCtrl.getOneByTag(tag.strip()))
                    else:
                        # Create the tag if it does not exist
                        newTag = Tag(tag.strip(), 1)
                        TagCtrl.add(newTag)
                        aTags.append(newTag)

                # Load the softwares
                for software in softwares:
                    aSoftwares.append(SftCtrl.getOne(software))

                # Load the files


                model.label       = kwargs['label']
                model.description = kwargs['description']
                model.tags        = aTags
                model.vertices    = kwargs['vertices']
                model.faces       = kwargs['faces']
                model.textured    = textured
                model.softwares   = aSoftwares
                model.user        = cherrypy.session['user'].id

                MdlCtrl.update(model)

                # Update the tag count & delete them if necessary
                TagCtrl.countTags()

            return super(ModelController, self).render(
                'model/form',
                title = 'Edit model',
                id = model.id,
                label = model.label,
                description = model.description,
                tags = tags,
                vertices = model.vertices,
                faces = model.faces,
                textured = model.textured,
                finalImage = finalImage,
                softwares = softwares,
                allSoftwares = allSoftwares,
                allFileTypes = allFileTypes,
                aFileTypes   = aFileTypes,
                fileTypes = fileTypes,
                files   = files,
                elements = kwargs,
                messages = messages,
                action = '/model/edit?id=' + str(model.id))

        else:
            cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    @requireAuthentication
    def delete(self, id):
        model = MdlCtrl.getOne(id)

        static   = cherrypy.request.app.config['/']['tools.staticdir.root']

        # Remove the current image
        if not model.images == '/model/img/default.jpg' and os.path.isfile(static + '/public/assets' + model.images) :
            os.remove(static + '/public/assets' + model.images)

        MdlCtrl.delete(model)

        # Update the tag count & delete them if necessary
        TagCtrl.countTags()

        raise cherrypy.HTTPRedirect('/')

