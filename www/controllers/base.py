import os
from os import path
import cherrypy

from includes.model.model import User, Model, Software

from includes.classes.controller.userController     import UserController     as UsrCtrl
from includes.classes.controller.modelController    import ModelController    as MdlCtrl
from includes.classes.controller.softwareController import SoftwareController as SftCtrl

from includes.templates import serve

class Base:

    def render(self, templateName, **kwargs):

        if 's' in kwargs:
            searched = kwargs['s']
        else:
            searched = ''

        # Add elements used in the template
        if 'user' in cherrypy.session:
            # Update the user stored instance
            cherrypy.session['user'] = UsrCtrl.getOne(cherrypy.session['user'].id)

            user =  cherrypy.session['user']
        else:
            user = None

        backgrounds = [f for f in os.listdir('./public/assets/img/default_backgrounds')]

        return serve(templateName,
                     backgrounds = backgrounds,
                     user        = user,
                     searched    = searched,
                     **kwargs)
