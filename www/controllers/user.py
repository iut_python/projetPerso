import os
import cherrypy
import re

from includes.model.model import User
from includes.classes.controller.userController import UserController as UsrCtrl

from includes.templates import serve

from controllers.base import Base

class UserController(Base):
    @cherrypy.expose
    def login(self, pseudonym = None, password = None):

        # If the form is not empty
        if (not pseudonym == None):
            messages = []

            # Get the user corresponding to the
            # given arguments (returns False when none)
            user = UsrCtrl.getByCredential(pseudonym, password)

            # If the user does not exist
            if not user:
                messages.append('Pseudonym or password invalid')

                # Show the login view with a message error
                return super(UserController, self).render(
                    'login',
                    title   = 'Login failed',
                    messages = messages,
                    pseudonym = pseudonym)
            else:
                # Add an entry to the session dictionnary
                cherrypy.session['user'] = user
                raise cherrypy.HTTPRedirect("/")
        else:
            # If the user is already logged in
            if 'user' in cherrypy.session and len(cherrypy.session['user']) > 0:
                raise cherrypy.HTTPRedirect("/")
            else:
                return super(UserController, self).render(
                    'login',
                    title = 'Login')

    @cherrypy.expose
    def signup(self,
               pseudonym    = None,
               surname      = None,
               name         = None,
               mail         = None,
               password     = None,
               passwordConf = None):

        # If the form is not yet submited
        if (pseudonym == None):
            return super(UserController, self).render(
                'signup',
                title   = 'Sign up',
                pseudonym    = '',
                surname      = '',
                name         = '',
                mail         = '')
        else:
            messages = []
            error   = False

            # Check if all required fields are filled
            if pseudonym == '' or mail == '' or password == '' or passwordConf == '':
               error = True
               messages.append('Required fields are not provided')

            else:

                if password != passwordConf:
                    error = True
                    messages.append('Passwords do not match')

                if len(password) < 5:
                    error = True
                    messages.append('Password is too short (5 chars min)')

                # Check if the email is well formed
                if not re.match(r'[^@]+@[^@]+\.[^@]+', mail):
                    error = True
                    messages.append('Invalid email address')

                # Check if the pseudonym is unique
                if UsrCtrl.pseudoExists(pseudonym):
                    error = True
                    messages.append('This pseudonym is already in use')

                # Check if the email is unique
                if UsrCtrl.mailExists(mail):
                    error = True
                    messages.append('This email address is already in use')

            if error:
                return super(UserController, self).render(
                    'signup',
                    title     = 'Sign up',
                    pseudonym = pseudonym,
                    surname   = surname,
                    name      = name,
                    mail      = mail,
                    messages  = messages)
            else:
                # Insert the user in the database
                user = User(surname, name, pseudonym, mail, password)
                UsrCtrl.add(user)

                # Log the user in
                self.login(pseudonym, password)


    @cherrypy.expose
    def logout(self):
        if 'user' in cherrypy.session:
            # The user session is popped out
            cherrypy.session.pop('user', None)
        raise cherrypy.HTTPRedirect("/")
