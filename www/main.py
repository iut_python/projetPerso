import os
import cherrypy

from includes.templates import serve
from includes.classes.controller.tagController import TagController as TagCtrl

from controllers.base  import Base
from controllers.model import ModelController
from controllers.user  import UserController

config = os.path.join(os.path.dirname(__file__), 'config/server.conf')

class Main(Base):

    # Declare controllers used to handle /model/ and /user/
    model = ModelController()
    user  = UserController()

    @cherrypy.expose
    def index(self):
        return super(Main, self).render(
            'index',
            title = 'Home page',
            tags  = TagCtrl.getAll())

if __name__ == '__main__':
    cherrypy.quickstart(Main(), config=config)
