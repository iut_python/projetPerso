import cherrypy
from functools import wraps

def requireAuthentication(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        if not 'user' in cherrypy.session:
            raise cherrypy.HTTPRedirect("/")
        return f(*args, **kwds)
    return wrapper
