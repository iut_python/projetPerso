from sqlalchemy import Table, Column, String, Integer, Boolean, ForeignKey, Date, create_engine

from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

from datetime import datetime
import hashlib

Base   = declarative_base()
engine = create_engine('sqlite:///data/bd.sql3')

sessionFactory = sessionmaker(bind = engine)
Session = scoped_session(sessionFactory)

session = Session()

class User(Base):
    __tablename__ = "user"

    id        = Column(Integer, primary_key = True)
    surname   = Column(String)
    name      = Column(String)
    pseudonym = Column(String, nullable = False)
    mail      = Column(String)
    password  = Column(String, nullable = False)

    models    = relationship("Model")

    def __init__(self, surname, name, pseudonym, mail, password):
        self.surname   = surname
        self.name      = name
        self.pseudonym = pseudonym
        self.mail      = mail
        self.password  = hashlib.sha512(password.encode('utf-8')).hexdigest()

class Model(Base):
    __tablename__ = "model"

    id          = Column(Integer, primary_key = True)
    label       = Column(String,  nullable = False)
    description = Column(String)
    vertices    = Column(Integer, nullable = False)
    faces       = Column(Integer, nullable = False)
    textured    = Column(Boolean, nullable = False)
    images      = Column(String,  nullable = False)
    hits        = Column(Integer, default = 0)
    user        = Column(Integer, ForeignKey("user.id"))
    tags        = relationship('Tag',      secondary = "model_tag")
    softwares   = relationship("Software", secondary = "model_software")
    files       = relationship("File")

    def getUser(self):
        session = Session()
        return session.query(User).filter_by(id = self.user).first()

class Software(Base):
    __tablename__ = "software"

    id     = Column(Integer, primary_key = True)
    name   = Column(String, nullable = False)
    base   = Column(String, nullable = False)
    models = relationship(Model, secondary = 'model_software')

class Tag(Base):
    __tablename__ = "tag"

    id     = Column(Integer, primary_key = True)
    tag    = Column(String,  nullable = False)
    count  = Column(Integer, nullable = False, default = 1)
    models = relationship(Model,  secondary = 'model_tag')

    def __init__(self, tag, count):
        self.tag   = tag
        self.count = count

class FileType(Base):
    __tablename__ = "file_type"

    id    = Column(Integer, primary_key = True)
    label = Column(String, nullable = False)

class File(Base):
    __tablename__ = "file"

    id       = Column(Integer, primary_key = True)
    model    = Column(Integer, ForeignKey("model.id"))
    fileType = Column(Integer, ForeignKey("file_type.id"))
    path     = Column(String, nullable = False)
    size     = Column(Integer)
    hits     = Column(Integer, default = 0)

    def getFileType(self):
        session = Session()
        return session.query(FileType).filter_by(id = self.fileType).first()

class ModelTag(Base):
    __tablename__ = "model_tag"

    model = Column(Integer, ForeignKey("model.id"), primary_key = True)
    tag   = Column(Integer, ForeignKey("tag.id"),   primary_key = True)

class ModelSoftware(Base):
    __tablename__ = "model_software"

    model    = Column(Integer, ForeignKey("model.id"),    primary_key = True)
    software = Column(Integer, ForeignKey("software.id"), primary_key = True)

Base.metadata.create_all(engine)
