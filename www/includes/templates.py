from mako.template import Template
from mako.lookup import TemplateLookup

lookup = TemplateLookup(directories=['./public/views'],
                        module_directory='./tmp/mako_modules')

def serve(templateName, **kwargs):
    template = lookup.get_template(templateName+".mako")
    return template.render(**kwargs)
