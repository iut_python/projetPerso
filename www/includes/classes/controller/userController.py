from sqlalchemy.orm import sessionmaker

from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.exc import MultipleResultsFound

from includes.model.model import User, Base, engine, Session
import hashlib

Base.metadata.bind = engine

class UserController:

    @staticmethod
    def add(user):
        session = Session()
        session.add(user)
        session.commit()

    @staticmethod
    def addAll(users):
        session = Session()
        session.add_all(users)
        session.commit()

    @staticmethod
    def getOne(id):
        session = Session()
        return session.query(User).filter_by(id = id).first()

    @staticmethod
    def getByCredential(pseudonym, password):
        session = Session()
        hash = hashlib.sha512(password.encode('utf-8')).hexdigest()

        value = False

        try:
            value = session.query(User).filter_by(pseudonym = pseudonym,
                                                  password  = hash).one()
        except NoResultFound:
            pass
        except MultipleResultsFound:
            pass

        return value


    @staticmethod
    def getAll():
        session = Session()
        return session.query(User).all()

    @staticmethod
    def delete(user):
        session = Session()
        session.delete(user)
        session.commit()

    @staticmethod
    def pseudoExists(pseudo):
        session = Session()
        return session.query(User).filter_by(pseudonym = pseudo).count()

    @staticmethod
    def mailExists(mail):
        session = Session()
        return session.query(User).filter_by(mail = mail).count()
