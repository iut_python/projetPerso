from sqlalchemy.orm import sessionmaker

from includes.model.model import engine, Session, Base, File, FileType

Base.metadata.bind = engine

class FileController:

    @staticmethod
    def add(file):
        session = Session()
        session.add(model)
        session.commit()

    @staticmethod
    def getOne(id):
        session = Session()
        return session.query(File).filter_by(id = id).first()

    @staticmethod
    def getOneType(id):
        session = Session()
        return session.query(FileType).filter_by(id = id).first()

    @staticmethod
    def getAll():
        session = Session()
        return session.query(File).all()

    @staticmethod
    def getAllTypes():
        session = Session()
        return session.query(FileType).all()

    @staticmethod
    def delete(file):
        session = Session()
        session.delete(file)
        session.commit()

    @staticmethod
    def update(file):
        session = Session()
        oldFile  = session.query(File).filter_by(id = file.id).first()

        oldFile.model    = file.model
        oldFile.fileType = file.fileType
        oldFile.path     = file.path
        oldFile.size     = file.size
        oldFile.hits     = file.hits

        session.commit()
