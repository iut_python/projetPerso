from sqlalchemy     import desc
from sqlalchemy.orm import sessionmaker

from includes.model.model import Tag, Base, engine, Session
from includes.classes.controller.modelController import ModelController as MdlCtrl

Base.metadata.bind = engine

class TagController:

    @staticmethod
    def add(tag):
        session = Session()
        session.add(tag)
        session.commit()

    @staticmethod
    def addAll(tags):
        session = Session()
        session.add_all(tags)
        session.commit()

    @staticmethod
    def getOne(id):
        session = Session()
        return session.query(Tag).filter_by(id = id).first()

    @staticmethod
    def getOneByTag(tag):
        session = Session()
        return session.query(Tag).filter_by(tag = tag).first()

    @staticmethod
    def getAll():
        session = Session()
        return session.query(Tag).order_by(desc(Tag.count)).all()

    @staticmethod
    def delete(tag):
        session = Session()
        session.delete(tag)
        session.commit()

    @staticmethod
    def update(tag):
        session = Session()
        oldTag  = session.query(Tag).filter_by(id = tag.id).first()

        oldTag.tag   = tag.tag
        oldTag.count = tag.count

        session.commit()

    @staticmethod
    def tagExists(tag):
        session = Session()
        return session.query(Tag).filter_by(tag = tag).count() > 0

    @staticmethod
    def countTags():
        for tag in TagController.getAll():
            tag.count = len(tag.models)
            TagController.update(tag)

        session = Session()
        for tag in session.query(Tag).filter_by(count = 0).all():
            TagController.delete(tag)
