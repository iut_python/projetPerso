from sqlalchemy.orm import sessionmaker

from includes.model.model import Software, Base, engine, Session

Base.metadata.bind = engine

class SoftwareController:

    @staticmethod
    def add(software):
        session = Session()
        session.add(software)
        session.commit()

    @staticmethod
    def addAll(softwares):
        session = Session()
        session.add_all(softwares)
        session.commit()

    @staticmethod
    def getOne(id):
        session = Session()
        return session.query(Software).filter_by(id = id).first()

    @staticmethod
    def getAll():
        session = Session()
        return session.query(Software).all()

    @staticmethod
    def delete(software):
        session = Session()
        session.delete(software)
        session.commit()

    @staticmethod
    def nameExists(name):
        session = Session()
        return session.query(Software).filter_by(name = name).count()
