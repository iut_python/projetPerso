from sqlalchemy.orm import sessionmaker

from includes.model.model import Model, Tag, Base, engine, Session

Base.metadata.bind = engine

class ModelController:

    @staticmethod
    def add(model):
        session = Session()
        session.add(model)
        session.commit()

    @staticmethod
    def getOne(id):
        session = Session()
        return session.query(Model).filter_by(id = id).first()

    @staticmethod
    def getAll():
        session = Session()
        return session.query(Model).all()

    @staticmethod
    def getTagged(tags):
        session = Session()
        return session.query(Model).filter(Model.tags.any(Tag.tag.in_(tags)))

    @staticmethod
    def delete(model):
        session = Session()
        session.delete(model)
        session.commit()

    @staticmethod
    def update(model):
        session = Session()
        oldModel  = session.query(Model).filter_by(id = model.id).first()

        oldModel.label       = model.label
        oldModel.description = model.description
        oldModel.tags        = model.tags
        oldModel.vertices    = model.vertices
        oldModel.faces       = model.faces
        oldModel.textured    = model.textured
        oldModel.images      = model.images
        oldModel.hits        = model.hits
        oldModel.user        = model.user

        session.commit()
