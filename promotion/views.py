# -*- coding: utf-8 -*-

# Standard library imports
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth import login as auth_login
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from members.models import Member
from members.forms import AuthenticationForm
from members.forms import RegistrationForm


def index(request):

    messaging_url = "/dashboard/index/"

    authentication_form = AuthenticationForm(request, prefix='authentication')
    registration_form = RegistrationForm(prefix='registration')

    if request.method == "POST":
        user = None
        if "login" in request.POST:
            authentication_form = AuthenticationForm(request, data=request.POST, prefix='authentication')
            if authentication_form.is_valid():
                # Okay, security check complete. Get the user.
                user = authentication_form.get_user()
        elif "register" in request.POST:
            registration_form = RegistrationForm(data=request.POST, prefix='registration')
            if registration_form.is_valid():
                username = registration_form.cleaned_data.get("username")
                password = registration_form.cleaned_data.get("password")
                email = registration_form.cleaned_data.get("email")

                user = User(username=username,
                            email=email)
                user.set_password(password)
                user.save()
                Member.objects.create(user=user)

                user = authenticate(username=username,
                                    password=password)

        if user is not None:
            auth_login(request, user)
            return HttpResponseRedirect(messaging_url)

    context = {'authentication_form': authentication_form,
               'registration_form': registration_form}
    return render(request, "promotion/index.html", context)
